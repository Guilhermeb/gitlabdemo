import ConfigParser, os, sys, getopt
import argparse
from subprocess import call, check_output, check_call
from subprocess import CalledProcessError

platforms = {
        'qt4': [
                'qmake',
                'make -j2'
                ]
        }

argumentParser = argparse.ArgumentParser(description='building tool')
argumentParser.add_argument('-p','--platform', help=platforms.keys, default='', required=False)

args = argumentParser.parse_args()
platformName = args.platform
if platformName not in platforms.keys():
        print("plataforma %s desconhecida" % platformName)
        exit(1)

commands = platforms[platformName]

for command in commands:
        try:
                check_call(command, shell=True)
        except CalledProcessError as error:
                print ("Erro: %s" % error)
                exit(1)

